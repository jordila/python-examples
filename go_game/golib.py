# -*- coding: utf-8 -*-
# What is this? ^^^
# Read more here:
# https://stackoverflow.com/questions/6289474/working-with-utf-8-encoding-in-python-source#6289494

from engine import put_stone

def print_board(size, board):
    """
    Espera un tablero como una lista de diccionarios:
    {x: _ , y: _, color: _ }
    Donde color es un caracter a pintar.

    NOTE: Los caracteres raros que usa:
    https://en.wikipedia.org/wiki/Box-drawing_character
    """

    # Matriz bidimensional vacía, se construye con dos list-comprehension
    # anidados:
    drawing = [ [ ' ' for i in range(size) ] for j in range(size) ]

    # Se llenan los valores obtenidos del tablero
    for stone in board:
        drawing[stone["x"]][stone["y"]] = stone["color"]

    # Se prepara el pintado:
    # Borde superior: ╔══════════╗
    topborder    = '╔'+ ''.join([ '═' for i in range(size) ]) +'╗'
    # Borde inferior: ╚══════════╝
    bottomborder = '╚'+ ''.join([ '═' for i in range(size) ]) +'╝'

    print(topborder)
    # Se pinta la parte interior, con los contenidos del `drawing`
    for row in drawing:
        print( '║' + ''.join(row) + '║' )
    print(bottomborder)


def process_input(ans):
    """ Extrae respuestas válidas del input de usuario.
    Se considera que el input de usuario es o `pass` o dos números enteros
    separados por un espacio.

    Devuelve:
        - None si la respuesta ha sido `pass`
        - Una tupla con forma (x, y) si la respuesta son coordenadas.
    """
    if ans == "pass":
        return None

    # Separa por espacio
    separated = ans.split(' ')

    # Comprueba que son dos coordenadas
    if len(separated) != 2:
        raise ValueError # Si no son dos, lanza una excepción

    # Convierte a Integer y el MapObject resultante a Tupla
    return tuple( map(lambda x: int(x), separated) )

def get_user_input():
    """ Recibe el input del usuario y lo devuelve procesado.
    Devuelve:
        - None si la respuesta es `pass`
        - Una tupla con forma (x, y) si ha dado unas coordenadas.
    """
    answer = input('Type coordinates or `pass`: ')

    # Uso una estructura tipo Try-Catch (en Python se llama Try-Except)
    # Intenta ejecutar el código del bloque Try.
    #   - Si falla con alguna excepción -> ejecuta el bloque except
    #
    #   Investigad aquí:
    #   https://wiki.python.org/moin/HandlingExceptions
    try:
        return process_input(answer)
    except:
        print("Invalid input, try again")
        return get_user_input() # Función recursiva!

def get_color(numb):
    """ Devuelve "B" si `numb` es par, "W" si es impar

    NOTE: Asumo que los turnos empiezan en cero
    """
    return ['B', 'W'][ numb % 2 ] # Truco sucio y molón, pensad y preguntad

def is_ko(board, history):
    """ Comprueba si ocurre el Ko.
    Devuelve True si ha ocurrido Ko, False si no ha ocurrido.
    """
    # Ocurre Ko si el tablero actual está en la historia
    return board in history[:-1]

def pos_filled(stone, board):
    """ Comprueba si la posición del tablero está ocupada.
    Devuelve True si está ocupada, False si no lo está.
    """
    for i in board:
        if i['x'] == stone['x'] and i['y'] == stone['y']:
            return True
    return False

def pos_in_board(stone, size):
    """ Comprueba si la posición está en el tablero.
    Devuelve True si lo está, False si no lo está.
    """
    if stone["x"] >= size or stone["y"] >= size:
        return False
    return True

if __name__ == "__main__":

    size = 19
    history = [ [] ]

    # Mientras que no haya al menos tres turnos o que los tres últimos turnos
    # NO tengan el mismo resultado
    #
    # Utiliza una comparación triple, lo explica bien aquí:
    #   https://docs.python.org/2.3/ref/comparisons.html
    # NOTE: es la docu de python2.3 pero este caso pasa igual hoy, pongo
    # ese link porque lo explica bien al principio
    #
    # En Python3 se pueden comparar objetos complejos como estos
    while len(history)<3 or not history[-1] == history[-2] == history[-3]:

        # Copia el último paso del historial para poder transformarlo y
        # jugar con él.
        # Remember: ¡Todo es una referencia!
        #   -> ¡No queremos estropear el historial!
        board = [ dict(i) for i in history[-1] ]
        print_board( size, board )              # Pinta el tablero

        # Comprobación de si la piedra está bien puesta:
        #   - Está dentro
        #   - No está encima de ninguna
        #   - No genera un Ko
        while True:

            # Interactua con el jugador
            ans = get_user_input()              # Pregunta el movimiento
            print(ans)                          # Pinta el input recibido

            if ans == None:
                # Ha pasado, repite el tablero y pasa de turno
                break

            # Ha introducido una coordenada

            # Construye una piedra desde una coordenada
            stone = { "x":      ans[0],
                      "y":      ans[1],
                      "color":  get_color(len(history)) }

            # Comprueba que la coordenada esté dentro del tablero
            if not pos_in_board(stone, size):
                print("That position is out of the board")
                continue

            # Comprueba si está ocupada la posición
            if pos_filled(stone, board):
                # Está ocupada, pide input de nuevo
                print("That position is already filled! Try again:")
                continue

            # Coloca la piedra en el tablero y lo procesa
            processed_board = put_stone( board, stone, size )

            # Ordena para que se puedan comparar
            # [1,2] == [1,2] => True
            # [1,2] == [2,1] => False
            # El algoritmo para ordenar es X*Size+Y lo que genera
            # identificadores ordenados de todas las posiciones, la (0,0)
            # siempre será la primera y la (size,size) la última.
            processed_board = sorted(processed_board, key=lambda x: x['y']*size + x['x'])

            # Comprueba si ha habido un Ko
            if is_ko(processed_board, history):
                # Hay Ko, pide input de nuevo
                print("That move is a KO! Try again:")
                continue

            # La piedra está bien
            # `processed_board` está bien, lo guardamos como válido
            board = processed_board
            break

        history.append(board)
